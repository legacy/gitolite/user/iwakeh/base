<!-- xml-snippets to be inserted in Metrics Java Projects -->
<!-- It's a snippet, i.e. not valid xml in itself.        -->

  <!-- generic properties -->
  <property name="buildresources" value="src/build/java" />
  <property name="sources" value="src/main/java"/>
  <property name="libs" value="lib"/>
  <property name="generated" value="generated"/>
  <property name="resources" value="src/main/resources"/>
  <property name="dist" value="${generated}/dist"/>
  <property name="signed" value="${dist}/signed"/>
  <property name="classes" value="${generated}/classes/"/>
  <property name="docs" value="${generated}/javadoc"/>
  <property name="testsources" value="src/test/java"/>
  <property name="testresources" value="src/test/resources/"/>
  <property name="testresult" value="${generated}/test-results"/>
  <property name="coverageresult" value="${generated}/coverage-report/"/>
  <property name="instrument" value="${generated}/instrument/"/>
  <property name="testclasses" value="${generated}/testclasses/"/>
  <property name="cobertura.ser.file" value="${basedir}/cobertura.ser" />
  <property name="jarfile" value="${name}-${release.version}.jar" />
  <property name="jarsourcesfile"
            value="${name}-${release.version}-sources.jar" />
  <property name="jardocsfile"
            value="${name}-${release.version}-javadoc.jar" />
  <property name="source-and-target-java-version" value="1.7" />
  <property name="release.tarball"
            value="${dist}/${name}-${release.version}.tar.gz" />
  <property file="build.properties" />
  <property name="emptymanifest" value="${generated}/emptymanifest" />

  <!-- generic path definitions -->
  <path id="classpath">
    <pathelement path="${classes}"/>
    <fileset dir="${libs}">
      <patternset refid="runtime" />
    </fileset>
  </path>

  <path id="test.classpath">
    <pathelement path="${classes}"/>
    <pathelement path="${resources}"/>
    <pathelement path="${testclasses}"/>
    <pathelement path="${testresources}"/>
    <fileset dir="${libs}">
      <patternset refid="runtime" />
    </fileset>
    <fileset dir="${libs}">
      <include name="junit4-4.11.jar"/>
      <include name="hamcrest-all-1.3.jar"/>
      <include name="logback-core-1.1.2.jar" />
      <include name="logback-classic-1.1.2.jar" />
    </fileset>
  </path>

  <path id="checkstyle.classpath" >
    <fileset dir="${libs}">
      <include name="checkstyle-6.17-all.jar" />
    </fileset>
  </path>

  <path id="cobertura.classpath">
    <fileset dir="${libs}">
      <include name="cobertura-2.1.1.jar" />
      <include name="slf4j-api-1.7.7.jar" />
      <include name="commons-lang3-3.3.2.jar" />
      <include name="asm4-5.0.3.jar" />
      <include name="asm4-util-5.0.3.jar" />
      <include name="asm4-tree-5.0.3.jar" />
      <include name="asm4-commons-5.0.3.jar" />
      <include name="asm4-analysis-5.0.3.jar" />
      <include name="oro-2.0.8.jar" />
      <include name="logback-core-1.1.2.jar" />
      <include name="logback-classic-1.1.2.jar" />
    </fileset>
  </path>

  <path id="cobertura.test.classpath">
    <path location="${instrument}" />
    <path refid="test.classpath" />
    <path refid="cobertura.classpath" />
  </path>

  <!-- target definitions -->
  
  <target name="clean" >
    <delete includeEmptyDirs="true" quiet="true">
      <fileset dir="${generated}" defaultexcludes="false" includes="**" />
    </delete>
    <delete file="${cobertura.ser.file}" quiet="true" />
  </target>

  <target name="usage">
    <echo message="The main ant targets are:" />
    <echo message="'clean' removes all generated files." />
    <echo message="'compile' compiles sources to ${classes}." />
    <echo message="'test' runs all tests (see ${testresult})." />
    <echo message="'docs' creates all javadoc in ${docs}." />
    <echo message="'checks' checks coding style (see ${generated}/checkstyle_report.txt)." />
    <echo message="'coverage' checks coverage (see ${coverageresult})." />
    <echo message="'tar' creates a release tarball in ${dist}." />
  </target>

  <target name="submoduleupdate" >
    <exec executable="git" failonerror="false">
      <arg value="submodule" />
      <arg value="update" />
      <arg value="--remote" />
      <arg value="--merge" />
    </exec>
  </target>

  <target name="init" depends="submoduleupdate" >
    <mkdir dir="${classes}"/>
    <mkdir dir="${testclasses}"/>
    <mkdir dir="${docs}"/>
    <mkdir dir="${testresult}"/>
    <mkdir dir="${instrument}"/>
    <mkdir dir="${signed}"/>
    <manifest file="${emptymanifest}" />
  </target>

  <target name="compile"
          depends="init">
    <javac destdir="${classes}"
           srcdir="${sources}"
           source="${source-and-target-java-version}"
           target="${source-and-target-java-version}"
           debug="true"
           deprecation="true"
           optimize="false"
           failonerror="true"
           includeantruntime="false">
      <classpath refid="classpath"/>
    </javac>
  </target>

  <target name="testcompile" depends="init">
    <javac destdir="${testclasses}"
           srcdir="${testsources}"
           source="${source-and-target-java-version}"
           target="${source-and-target-java-version}"
           debug="true"
           deprecation="true"
           optimize="false"
           failonerror="true"
           includeantruntime="false">
      <classpath refid="test.classpath"/>
    </javac>
  </target>

  <target name="docs">
    <tstamp>
      <format property="copyyear"
              pattern="yyyy"
              timezone="UTC" />
    </tstamp>
    <javadoc destdir="${docs}"
             footer="&amp;copy; ${copyyear} The Tor Project"
             doctitle="${javadoc-title}"
             overview="${basedir}/${resources}/overview.html"
             use="true"
             windowtitle="${javadoc-title}">
      <classpath refid="classpath"/>
      <fileset dir="${sources}">
        <exclude name="${javadoc-excludes}"/>
      </fileset>
    </javadoc>
  </target>

  <target name="test" depends="compile,testcompile">
    <junit fork="true"
           haltonfailure="true"
           printsummary="on">
      <jvmarg value="-DLOGBASE=${generated}/test-logs"/>
      <jvmarg value="-Djava.security.policy=${buildresources}/junittest.policy"/>
      <jvmarg value="-Djava.security.manager"/>
      <classpath refid="test.classpath"/>
      <formatter type="plain" usefile="false"/>
      <batchtest>
        <fileset dir="${testclasses}"
                 includes="**/*Test.class"/>
      </batchtest>
    </junit>
  </target>

  <taskdef resource="com/puppycrawl/tools/checkstyle/ant/checkstyle-ant-task.properties">
    <classpath refid="checkstyle.classpath" />
  </taskdef>
  <target name="checks" depends="compile">
    <checkstyle config="${buildresources}/metrics_checks.xml">
      <fileset dir="${sources}"
               includes="**/*.java" />
      <fileset dir="${testsources}"
               includes="**/*.java" />
      <classpath>
        <path refid="classpath" />
        <path refid="checkstyle.classpath" />
      </classpath>
      <formatter type="plain"
                 toFile="${generated}/checkstyle_report.txt"/>
    </checkstyle>
    <exec executable="cat" outputproperty="checkstyle.result">
      <arg value="${generated}/checkstyle_report.txt" />
    </exec>
    <fail message="Checkstyle complaints: ${checkstyle.result}" >
      <condition>
        <not>
          <length string="${checkstyle.result}"
                  length="29" />
        </not>
      </condition>
    </fail>
  </target>

  <target name="gitrev" >
    <exec executable="git" outputproperty="git.revision">
      <arg value="rev-parse" />
      <arg value="--short" />
      <arg value="HEAD" />
    </exec>
  </target>

  <patternset id="base.jar" >
    <patternset refid="runtime" />
  </patternset>
  <patternset id="empty" excludes="*"/>
  <target name="jar" depends="compile,docs,gitrev" >
    <property name="manifestattributes" value="${generated}/additional" />
    <manifest file="${manifestattributes}" >
      <attribute name="Main-Class" value="${project-main-class}" />
    </manifest>
    <condition property="manifestfile"
               value="${manifestattributes}"
               else="${emptymanifest}" >
        <isset property="project-main-class"/>
    </condition>
    <condition property="jarpattern"
               value="${jarpatternprop}"
               else="runtime" >
        <isset property="jarpatternprop"/>
    </condition>
    <antcall target="jarring" >
      <param name="destfile" value="${dist}/${jarfile}" />
      <param name="usepath" value="${libs}" />
      <param name="usepattern" value="${jarpattern}" />
      <param name="usebase" value="${classes}" />
    </antcall>
    <property name="manifestfile" value="${emptymanifest}" />
    <antcall target="jarring" >
      <param name="destfile" value="${dist}/${jarsourcesfile}" />
      <param name="usepath" value="${libs}" />
      <param name="usepattern" value="empty" />
      <param name="usebase" value="${basedir}/src" />
    </antcall>
    <antcall target="jarring" >
      <param name="destfile" value="${dist}/${jardocsfile}" />
      <param name="usepath" value="${libs}" />
      <param name="usepattern" value="empty" />
      <param name="usebase" value="${docs}" />
    </antcall>
  </target>

  <target name="jarring" >
    <jar destfile="${destfile}"
         basedir="${usebase}"
         manifest="${manifestfile}" >
      <fileset dir="${resources}" includes="${jarincludes}" />
      <restrict>
        <not>
          <name name="META-INF/*" />
        </not>
        <archives>
          <zips>
            <fileset dir="${usepath}">
              <patternset refid="${usepattern}" />
            </fileset>
          </zips>
        </archives>
      </restrict>

      <manifest>
        <attribute name="Created-By" value="The Tor Project" />
        <attribute name="Implementation-Title" value="${implementation-title}"/>
        <attribute name="Implementation-Version"
                   value="${release.version}-${git.revision}"/>
      </manifest>
    </jar>
  </target>

  <target name="signall" depends="jar">
    <condition property="tobesigned"
               value="${jarfile} ${jarsourcesfile} ${jardocsfile} ${additional2sign}"
               else="${jarfile} ${jarsourcesfile} ${jardocsfile}" >
        <isset property="additional2sign"/>
    </condition>
    <signjar alias="${jarsigner.alias}"
             storepass="${jarsigner.storepass}"
             sigalg="SHA256withRSA"
             destdir="${signed}"
             digestalg="SHA-256"
             tsaurl="http://timestamp.digicert.com"
             lazy="true">
      <path>
        <fileset dir="${dist}"
                 includes="${tobesigned}"/>
      </path>
    </signjar>
    <fail message="Signing of at least one of ${tobesigned} failed." >
      <condition>
        <not>
          <and>
            <issigned file="${signed}/${jarfile}"/>
            <issigned file="${signed}/${jarsourcesfile}"/>
            <issigned file="${signed}/${jardocsfile}"/>
            <or>
              <issigned file="${signed}/${additional2sign}"/>
              <not>
                <isset property="additional2sign"/>
              </not>
            </or>
          </and>
        </not>
      </condition>
    </fail>

  </target>

  <target name="tar" depends="signall">
    <tar destfile="${release.tarball}" compression="gzip">
      <tarfileset dir="." prefix="${name}-${release.version}">
        <include name="${signed}/*" />
        <include name="build.xml" />
        <include name="LICENSE" />
        <include name="CERT" />
        <include name="*.md" />
      </tarfileset>
      <tarfileset dir="${basedir}/src"
                  prefix="${name}-${release.version}/src" />
      <tarfileset dir="${libs}"
                  prefix="${name}-${release.version}/lib" />
    </tar>
  </target>

  <taskdef classpathref="cobertura.classpath" resource="tasks.properties" />
  <target name="coverage" depends="compile,testcompile">
    <delete file="${cobertura.ser.file}" quiet="true" />
    <copy todir="${instrument}" >
      <fileset dir="${classes}"/>
    </copy>
    <cobertura-instrument ignoreTrivial="true">
      <fileset dir="${instrument}">
        <include name="**/**/*.class" />
      </fileset>
    </cobertura-instrument>
    <junit fork="true" haltonfailure="false" printsummary="on">
      <sysproperty key="net.sourceforge.cobertura.datafile"
                   file="${cobertura.ser.file}" />
      <jvmarg value="-DLOGBASE=${generated}/testcoverage-logs"/>
      <jvmarg value="-Djava.security.policy=${buildresources}/junittest.policy"/>
      <jvmarg value="-Djava.security.manager"/>
      <classpath refid="cobertura.test.classpath" />
      <formatter type="xml" />
      <batchtest toDir="${testresult}" >
        <fileset dir="${testclasses}"
                 includes="**/*Test.class"/>
      </batchtest>
    </junit>
    <cobertura-report format="html" destdir="${coverageresult}" >
      <fileset dir="${sources}">
        <include name="**/*.java" />
      </fileset>
    </cobertura-report>
    <antcall target="coverage-check" />
  </target>

